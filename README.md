# CottageNeedlecrafts
The project is live here:
https://07ywnqdsh6.execute-api.eu-west-2.amazonaws.com/dev

# Motivation
This is a personal project to learn how to use React and Django Rest API and AWS lambda. The project is a clone of a ecommerce craft website https://cottageneedlecrafts.org/. I chose to make an ecommerce website as I thought this would allow me to do something interesting with React on the front end. 

# Build
The project uses the Django rest framework on the backend. Ideally this would be reading from a relational database, at the moment the data is being read in from a file (products.py). This also means that a serializer is not being used. The API currently only has end points for products. 

The front end is built using React, there is currently an issue with state management which I want to resolve using Redux.

# Deployment
The project is deployed using Amazon Web Services as a lambda function. This is done using Zappa. The react app is converted to static files and put with all other project static files on a S3 bucket.

# Extra things that can be added
- Have a relational database on the back end (probably hosted using Amazon Relational Database Service (RDS)). And add serializer to the Django Rest Framework set-up.
- Build out the api for other parts of the website for example administration
- Sort out state management for basket and authentification.
- Sort out state management with Redux






